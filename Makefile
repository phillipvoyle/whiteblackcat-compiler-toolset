all: bin/wbclex bin/wbcparse

clean:
	rm -f bin/*
	rm -f src/wbcparse/wbcparse
	rm -f src/wbclex/wbclex
	$(MAKE) clean -C src/wbclex
	$(MAKE) clean -C src/wbcparse

bin/wbclex:
	$(MAKE) -C src/wbclex
	cp src/wbclex/wbclex bin

bin/wbcparse:
	$(MAKE) -C src/wbcparse
	cp src/wbcparse/wbcparse bin


